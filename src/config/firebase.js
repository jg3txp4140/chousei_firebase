import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyD1fym4oaBv2xpb32OZ9z1d8yeqFzzaanE",
  authDomain: "chousei-firebase-88027.firebaseapp.com",
  databaseURL: "https://chousei-firebase-88027.firebaseio.com",
  projectId: "chousei-firebase-88027",
  storageBucket: "",
  messagingSenderId: "773831331184",
  appId: "1:773831331184:web:c1fa79d1b58d670a93d3b5"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
